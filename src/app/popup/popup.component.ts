import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { DataListService } from '../data-list.service';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.css']
})
export class PopupComponent implements OnInit {

  constructor(private fr: FormBuilder, private _dataList: DataListService) { }

  get cName() {
    return this.Registration.get('cName');
  }

  get coName() {
    return this.Registration.get('coName');
  }

  get tt() {
    return this.Registration.get('tt');
  }

  get dep() {
    return this.Registration.get('dep');
  }

  get cat() {
    return this.Registration.get('cat');
  }

  onFormSubmit() {
    this.name.emit(this.Registration.value);
  }
  @Output() public name = new EventEmitter();


  Registration = this.fr.group({
    cName: ['', Validators.required],
    coName: ['', Validators.required],
    lName: ['', Validators.required],
    tc: ['', Validators.required],
    cNumber: ['', Validators.required],
    email: ['', Validators.required],
    tt: ['', Validators.required],
    dep: ['', Validators.required],
    at: ['', Validators.required],
    pr: ['', Validators.required],
    cat: ['', Validators.required],
    sub: ['', Validators.required]
  });
  public nameList;
  public contractList;
  public ticket;
  public Labour;
  public depart;
  public priority;
  public assigned;
  public category;
  public subCategory;
  ngOnInit() {
    this.nameList = this._dataList.getCname()
      .subscribe(data => this.nameList= data.Result1);
    this.contractList = this._dataList.getConame()   
      .subscribe(data => this.contractList = data.Result2);
    this.ticket = this._dataList.getTicket()
      .subscribe(data => this.ticket = data.Data);
    this.Registration.get("coName").valueChanges
      .subscribe((coName) => {
        this._dataList.getLabour(coName).subscribe(data => this.Labour = data );
      });
    this.Registration.get("tt").valueChanges
      .subscribe((tt) => {
        this._dataList.getDepartment(tt).subscribe(data => this.depart = data );
      });
    this.Registration.get("dep").valueChanges
      .subscribe(() => {
        this._dataList.getAssigned().subscribe(data => this.assigned = data.Data);
      });
    this.priority = this._dataList.getPriority()
      .subscribe(data => this.priority = data.Data);
    this.Registration.get('dep').valueChanges
      .subscribe((dep) => {
        this._dataList.getCategory(dep).subscribe(data => this.category = data);
      });
    this.Registration.get('cat').valueChanges
      .subscribe((cat) => {
        this._dataList.getSubCategory(cat).subscribe(data => this.subCategory = data);
      });
  }

}
