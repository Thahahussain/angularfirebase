import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class DataListService { 

  constructor(private http: HttpClient) { }

  storeData(val) {
    localStorage.setItem("key", JSON.stringify(val));
  }

  getmethod() {
    let val = JSON.parse(localStorage.getItem("key")) as any[];
     if (val == null ) {
       val = [];
     }
     return val;
  }
  
  getSomeData() {
   return JSON.parse(localStorage.getItem("key"));
  }
  getCname(): Observable<any> {
    return this.http.get("https://erp.arco.sa:65/api/GetTicketCustomerDetails?CustomerId=CIN0000150");
 }

  getConame():Observable<any> {
    return this.http.get("https://erp.arco.sa:65/api/GetTicketCustomerDetails?CustomerId=CIN0000150");
  }

  getLabour(coName):Observable<any> {
    return this.http.get(" https://erp.arco.sa:65//api/GetTicketIndContractAllEmployee?CustomerId=CIN0000150&ContractId=" + coName);
  }

  getTicket():Observable<any> {
    return this.http.get("https://erp.arco.sa:65//api/TickettypeList");
  }

  getDepartment(tt):Observable<any>{
    return this.http.get("https://erp.arco.sa:65/api/GetTicketAssignedToGroupByTicketTypeId?TicketTypeID=" + tt);
  }

  getAssigned():Observable<any> {
    return this.http.get(" https://erp.arco.sa:65//api/assigntoList" );
  }

  getPriority():Observable<any> {
    return this.http.get("https://erp.arco.sa:65//api/PriorityList");
  }

  getCategory(dep):Observable<any> {
    return this.http.get("https://erp.arco.sa:65//api/GetTicketGroupByDepatmentId?TicketAssignGroupId=" + dep);
  }

  getSubCategory(cat):Observable<any> {
   return this.http.get( " https://erp.arco.sa:65/api/SubGroupByGroup?id=" + cat);
  }

  
}
